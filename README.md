# catalog project

This project uses Quarkus, the Supersonic Subatomic Java Framework.

If you want to learn more about Quarkus, please visit its website: https://quarkus.io/ .

## Running the application in dev mode

You can run your application in dev mode that enables live coding using:
```
./mvnw quarkus:dev
```

## Packaging and running the application

The application can be packaged using `./mvnw package`.
It produces the `catalog-1.0.0-SNAPSHOT-runner.jar` file in the `/target` directory.
Be aware that it’s not an _über-jar_ as the dependencies are copied into the `target/lib` directory.

The application is now runnable using `java -jar target/catalog-1.0.0-SNAPSHOT-runner.jar`.

## Creating a native executable

You can create a native executable using: `./mvnw package -Pnative`.

Or, if you don't have GraalVM installed, you can run the native executable build in a container using: `./mvnw package -Pnative -Dquarkus.native.container-build=true`.

You can then execute your native executable with: `./target/catalog-1.0.0-SNAPSHOT-runner`

If you want to learn more about building native executables, please consult https://quarkus.io/guides/building-native-image.

CATATAN QUARKUS

1. brew install docker-machine-driver-hyperkit
2. sudo chown root:wheel /usr/local/bin/docker-machine-driver-hyperkit && sudo chmod u+s /usr/local/bin/docker-machine-driver-hyperkit
3. As part of the minishift start command, a CLI profile named minishift is also created. This profile, also known as a context, contains the configuration to communicate with your OpenShift cluster.
4. oc new-project quarkus —display-name=“Sample”
 -- -- 
 Now using project "quarkus" on server "https://192.168.42.230:8443".
 
 You can add applications to this project with the 'new-app' command. For example, try:
 
     oc new-app centos/ruby-25-centos7~https://github.com/sclorg/ruby-ex.git
 sh ./client-setup.sh
 to build a new example application in Ruby.
-- -- -
5. oc login -u system:admin
6. oc new-build quay.io/redhat/ubi-quarkus-native-runner --binary --name=quarkus-quickstart -l app=quarkus-quickstart
7. oc start-build quarkus-quickstart --from-file=./Documents/playground/getting-started/target/getting-started-1.0-SNAPSHOT-runner --follow
8. oc new-app quarkus-quickstart
9. oc expose svc/quarkus-quickstart
10. oc set resources dc/quarkus-quickstart --limits=memory=50Mi


how to start docker
minikube status
ps axf | grep docker | grep -v grep | awk '{print "kill -9 " $1}' | sudo sh
systemctl start docker
minikube status
and run docker build by sudo::: sudo docker build -f src/main/docker/Dockerfile.jvm -t quarkus/catalog-jvm .


./mvnw clean package -Dquarkus.kubernetes.deploy=true





# kubectl delete service postgres 
# kubectl delete deployment postgres
# kubectl delete configmap postgres-config
# kubectl delete persistentvolumeclaim postgres-pv-claim
# kubectl delete persistentvolume postgres-pv-volume

1. minikube start
2. kubectl -n pgo port-forward svc/postgres-operator 8443:8443
kubectl -n pgo port-forward svc/hippo 5432:5432
3. pgo create cluster hippo --replica-count=2, akan muncul hasil berikut
username: testuser password: <B_ja@(i.*GmWTiAZ<mcyB{N
4. pgo test hippo akan muncul service dan status nya
Handling connection for 8443

cluster : hippo
	Services
		primary (10.103.58.114:5432): UP
		replica (10.100.0.242:5432): UP
	Instances
		primary (hippo-56fc94474f-6vjtl): UP
		replica (hippo-ovjo-7c4476dc6f-pxc9g): UP
		replica (hippo-wldy-8d6fdcb4c-2mcxm): UP


5. kubectl -n pgo delete pods hippo-5df8c99fcf-nxz59, untuk simulate pod mati by delete namanya
6. ```psql -h localhost -U testuser --password -p 5432 postgres```, untuk koneksi ke DB. 
password nya bisa dilihat via ```pgo show user --show-system-accounts hippo```

CLUSTER USERNAME    PASSWORD                 EXPIRES  STATUS ERROR 
------- ----------- ------------------------ -------- ------ -----
hippo   abc         Hb5Q^Tsm7Z3]BfEiwIVy<g/: never    ok           
hippo   crunchyadm                           never    ok           
hippo   postgres    d1`aAZ0Um_l2UCPwiVF98q*h never    ok           
hippo   primaryuser 00X}JR1D+jY`F`>11)f,UnhN never    ok           
hippo   testuser    <B_ja@(i.*GmWTiAZ<mcyB{N never    ok  


7. pgo create user --username abc --selector name=hippo --managed --namespace pgo, untuk create user / bisa create by psql @user postgres
CLUSTER USERNAME PASSWORD                 EXPIRES  STATUS ERROR 
------- -------- ------------------------ -------- ------ -----
hippo   abc      Hb5Q^Tsm7Z3]BfEiwIVy<g/: never    ok           
8. masuk db untuk ```ALTER USER abc WITH SUPERUSER;``` dan  ```ALTER USER abc CREATEDB;``` dan ```ALTER USER abc WITH PASSWORD 'abc';```

unset DOCKER_HOST
./mvnw package -Pnative -Dquarkus.native.container-build=true && 
docker build -f src/main/docker/Dockerfile.native -t abc/catalog:1.0.0-SNAPSHOT .
kubectl apply -f target/kubernetes/kubernetes.yml 
./mvnw compile quarkus:dev
curl http://localhost:8080/nutrition/ -t
curl http://localhost:8080/nutrition/ -t 0.5
