package org.me.controller;

import org.jboss.resteasy.specimpl.ResponseBuilderImpl;
import org.me.service.FruitService;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/fruit")
public class FruitController {

    private final FruitService fruitService;

    public FruitController(FruitService fruitService) {
        this.fruitService = fruitService;
    }

    @GET
    @Path("/name/{name}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getFruitByName(@PathParam("name") String name) {
        ResponseBuilderImpl responseBuilder = new ResponseBuilderImpl();
        return responseBuilder
                .status(Response.Status.OK)
                .entity(fruitService.getFruitByName(name))
                .build();
    }
}
