package org.me.controller;

import org.jboss.resteasy.annotations.jaxrs.PathParam;
import org.jboss.resteasy.specimpl.ResponseBuilderImpl;
import org.me.model.Nutrition;
import org.me.service.NutritionService;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/nutrition")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class NutritionController {

    @Inject
    NutritionService nutritionService;

    @POST
    public Response add(Nutrition nutrition) {
        return nutritionService.addNutritionCatalog(nutrition);
    }

    @GET
    @Path("/")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllFruit() {
        ResponseBuilderImpl responseBuilder = new ResponseBuilderImpl();
        return responseBuilder
                .status(Response.Status.OK)
                .entity(nutritionService.get())
                .build();
    }

    @DELETE
    @Path("/")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteExpiredFruit() {
        ResponseBuilderImpl responseBuilder = new ResponseBuilderImpl();
        nutritionService.deleteALlExpiredFruit();
        return responseBuilder
                .status(Response.Status.OK)
                .build();
    }

    @DELETE
    @Path("/{name}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteFruitByName(@PathParam("name") String name) {
        ResponseBuilderImpl responseBuilder = new ResponseBuilderImpl();
        nutritionService.deleteFruitByName(name);
        return responseBuilder
                .status(Response.Status.OK)
                .build();
    }
}
