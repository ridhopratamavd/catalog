package org.me.service;

import org.me.model.Nutrition;

import javax.enterprise.context.ApplicationScoped;
import javax.transaction.Transactional;
import javax.ws.rs.core.Response;
import java.util.List;

import static io.quarkus.hibernate.orm.panache.PanacheEntityBase.delete;
import static io.quarkus.hibernate.orm.panache.PanacheEntityBase.persist;

@ApplicationScoped
public class NutritionService {

    @Transactional
    public Response addNutritionCatalog(Nutrition nutrition) {
        persist(nutrition);
        return Response.ok(nutrition).status(201).build();
    }

    public List<Nutrition> get() {
        return Nutrition.findAll().list();
    }

    @Transactional
    public void deleteALlExpiredFruit() {
        delete("description = 'string'");
    }

    @Transactional
    public void deleteFruitByName(String name) {
        Nutrition.deleteFruitByName(name);
    }
}
