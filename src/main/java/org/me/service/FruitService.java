package org.me.service;

import org.me.model.Fruit;
import org.me.repository.FruitRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class FruitService {

    @Inject
    private FruitRepository fruitRepository;

    public FruitService(FruitRepository fruitRepository) {
        this.fruitRepository = fruitRepository;
    }

    /*
     * still need to test
     * */
    public Fruit getFruitByName(String name) {
        return new Fruit(name, "description");
    }
}
