package org.me.repository;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import org.me.model.Fruit;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class FruitRepository implements PanacheRepository<Fruit> {
}
