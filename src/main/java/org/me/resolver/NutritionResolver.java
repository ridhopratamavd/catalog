package org.me.resolver;

import org.eclipse.microprofile.graphql.GraphQLApi;
import org.eclipse.microprofile.graphql.Query;
import org.me.model.Nutrition;

import java.util.List;

@GraphQLApi
public class NutritionResolver {

    @Query("allNutrition")
    public List<Nutrition> getAllNutrition() {
        return Nutrition.findAll().list();
    }
}
