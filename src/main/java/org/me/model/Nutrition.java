package org.me.model;

import io.quarkus.hibernate.orm.panache.PanacheEntity;
import io.quarkus.panache.common.Parameters;
import lombok.*;

import javax.persistence.Entity;

@Data
@NoArgsConstructor
@Builder
@AllArgsConstructor
@EqualsAndHashCode(callSuper=false)
@Entity
public class Nutrition extends PanacheEntity {

    String name;
    String description;

    public static void deleteFruitByName(String name) {
        delete("name = :name", Parameters.with("name", name));
    }
}
