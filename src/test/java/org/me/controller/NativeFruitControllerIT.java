package org.me.controller;

import io.quarkus.test.junit.NativeImageTest;
import org.me.controller.FruitControllerTest;

@NativeImageTest
public class NativeFruitControllerIT extends FruitControllerTest {

    // Execute the same tests but in native mode.
}
