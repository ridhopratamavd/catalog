package org.me.controller;

import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.mockito.InjectMock;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.me.model.Fruit;
import org.me.service.FruitService;
import org.mockito.Mockito;

import javax.ws.rs.core.Response;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;
import static org.mockito.ArgumentMatchers.anyString;

@QuarkusTest
public class FruitControllerTest {

    @InjectMock
    FruitService exampleService;

    @BeforeEach
    void setUp() {
        Mockito.when(exampleService.getFruitByName(anyString())).thenReturn(new Fruit("lucu", "desct"));
    }

    @Test
    public void getFruitByName_shouldReturnOk_whenDataExist() {
        given()
                .when().get("/fruit/name/harusnyaName")
                .then()
                .statusCode(Response.Status.OK.getStatusCode())
                .body(is("{\"description\":\"desct\",\"name\":\"lucu\"}"))
        ;
    }
}
