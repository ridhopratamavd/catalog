package org.me.controller;

import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.mockito.InjectMock;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.me.model.Nutrition;
import org.me.service.NutritionService;
import org.mockito.Mockito;

import javax.ws.rs.core.Response;

import static io.restassured.RestAssured.given;
import static org.mockito.ArgumentMatchers.any;

@QuarkusTest
class NutritionControllerTest {

    Nutrition nutrition = Nutrition.builder()
            .description("can make your skin fresh")
            .name("vitamin D")
            .build();

    @InjectMock
    NutritionService exampleService;

    @BeforeEach
    void setUp() {
        Mockito.when(exampleService.addNutritionCatalog(any(Nutrition.class))).thenReturn(Response.ok(nutrition).status(201).build());
    }

    @Test
    public void add_shouldCallNutritionServiceWithReturnStatusCreated_whenrequestComes() {
        given()
                .contentType(ContentType.JSON)
                .body(nutrition)
                .when()
                .post("/nutrition")
                .then()
                .statusCode(Response.Status.CREATED.getStatusCode());
    }
}
