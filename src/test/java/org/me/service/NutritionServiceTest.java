package org.me.service;

import io.quarkus.hibernate.orm.panache.PanacheQuery;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.me.model.Nutrition;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.core.Response;

import static org.junit.jupiter.api.Assertions.assertEquals;

@QuarkusTest
class NutritionServiceTest {

    @Inject
    NutritionService nutritionService;

    @AfterEach
    @Transactional
    public void tearDown() {
        Nutrition.deleteAll();
    }

    @Test
    void addNutritionCatalog_shouldSave_whenSuccessSendToDatabase() {
        Nutrition nutrition = Nutrition.builder()
                .name("nameee")
                .description("descriptionn")
                .build();

        final Response response = nutritionService.addNutritionCatalog(nutrition);
        final PanacheQuery<Nutrition> all = Nutrition.findAll();

        assertEquals(1, all.count());
        assertEquals(nutrition, response.getEntity());
    }
}
